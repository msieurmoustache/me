module.exports = {
  pathPrefix: '/',
  siteMetadata: {
    title: 'moussetache',
    description: 'Minimal personal page',
    author: 'moussetache',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: `${__dirname}/src/images`,
      },
    },
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'moussetache',
        short_name: 'moussetache',
        start_url: '/',
        background_color: `#32a852`,
        theme_color: `#32a852`,
        display: `minimal-ui`,
        icon: `src/images/me.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /svg/
        }
      }
    },
    'gatsby-plugin-sass',
    'gatsby-plugin-postcss'
  ]
};
