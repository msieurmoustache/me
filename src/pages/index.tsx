import React from "react";

import Layout from "@components/Layout/Layout";
import ShowcaseCard from "@components/Showcase/ShowcaseCard";
import { Link } from "gatsby";

export default function IndexPage () {
  return (
    <Layout>      
      <div>
        <div className="text-center mt-5">Also known as Félixe Bélanger-Robillard</div>
        <div className="text-center mb-5">This is what happens when you get an accidental bachelor's degree in Computer Science.</div>
      </div>      
      <div className="hurtubise hidden">
        <ShowcaseCard>
            <h2>In memoriam, Hurtubise circa 2017</h2>
            <iframe title="hurtubise-vs-msieurmoustache" src="https://lichess.org/embed/EuGXUtve#40?theme=auto" width={600} height={397} frameBorder={0} />
        </ShowcaseCard>
      </div>  
      <div className="flex flex-col items-center gap-5 transition-all">
        <div className="grid grid-cols-2 gap-3">
          <ShowcaseCard>
              <Link to="/trans-clues">
                On being trans
              </Link>
            </ShowcaseCard>
            <ShowcaseCard>
              <Link to="/ace-clues">
                On being ace
              </Link>
            </ShowcaseCard>
            <ShowcaseCard>
              <Link to="/autism-nights">
                Autism nights
              </Link>
            </ShowcaseCard>
        </div>
      </div>
    </Layout>
  );
} 