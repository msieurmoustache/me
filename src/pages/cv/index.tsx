import React from "react";

import Experience from '@components/CV/Experience';

export default function CV () {
    let jobs = [
        {
            timespan: "2021-2024",
            title: "Staff Developer, SRE",
            employer: "Workleap",
            description: "As a founding member of the SRE, I've been responsible of applying the SRE methodology to our softwares, mainly through the use of SLOs and enhanced observability in our systems to assist teams in both reliability and performance. A key part of my work both within SRE and my previous role has also been incident response management, both as incident manager and SME."
        },
        {
            timespan: "2018-2021",
            title: "Full-Stack Developer",
            employer: "Workleap (Officevibe)",
            description: "As a member of the operations' team, I was tasked with development of internal tools. I also accompanied teams in their journey to continuous deployment and guide them in the most complex bug investigations. "
        },
        {
            timespan: "2017-2018",
            title: "Programmer-Analyst",
            employer: "Institut National de Santé Publique du Québec",
            description: "Operational pilot for the Laboratory Information Management System (LIMS). I was in charge of all parts of the SDLC and support of the system."
        }
    ];

    let schooling = [
        {
            timespan: "2014-2017",
            name: "Baccalauréat en informatique et études des jeux vidéo",
            description: "The constrast with my previous studies might seem stark, but natural language theories transpose themselves really well to computer sciences and, more specifically, programming languages.",
            school: "Université de Montréal"
        },{
            timespan: "2012-2014",
            name: "Baccalauréat en enseignement du français au secondaire",
            description: "Never completed, but the skills I acquired help me to this day with training and mentoring.",
            school: "Université de Montréal"
        }
    ];

    let categoriesClasses = "text-xl font-bold bg-slate-200 dark:bg-slate-500 p-2 mt-5 mb-5";

    return (
        <div className="cv m-1" >
            <div className="w-9/12 m-auto">
                <header className="text-center">
                    <h1 className="text-2xl font-bold">Félixe Bélanger-Robillard</h1>
                    <p>(514) 431-9958 – f.brobillard@gmail.com</p>
                    <p>
                        <a className="text-blue-600" href="https://moussetache.ca/" target="_blank" rel="noreferrer noopener">moussetache.ca</a>
                    </p>
                </header>

                <h2 className={categoriesClasses}>Professional Work</h2>               
    
                <ul>
                    {jobs.map(x => <Experience timespan={x.timespan} name={x.title} place={x.employer} description={x.description} />)}
                </ul>
                
                <h2 className={categoriesClasses}>Studies</h2>
    
                <ul>
                    {schooling.map(x => <Experience timespan={x.timespan} name={x.name} place={x.school} description={x.description} />)}
                </ul>
    
                <h2 className={categoriesClasses}>Volunteer Work</h2>
    
                <ul>
                    <Experience 
                    timespan={"Since 2010"} 
                    name={"Scout"} 
                    place={"Association des aventuriers de Baden-Powell"} 
                    description={<span>
                            Currently in charge of the <a href="https://github.com/msieurmoustache/gaabp" target="_blank" rel="noopener noreferrer">website</a>. I'm also volunteering in a local group with the beavers. In the past, I've been in charge of volunteer training for the organisation, responsible for beaver age group and also was elected to its board.
                        </span>} />
                </ul>
            </div>
    
            <div className="w-9/12 m-auto">
                <h2 className={categoriesClasses}>Skills</h2>    
                <ul className="mt-0">
                    <div className="flex">
                        <div className="w-7/12 pr-1">
                            <p><em>Natural Languages</em> Français, English</p>
    
                            <p><em>Programming Languages</em> JavaScript, C#, Java, Python, MUMPS, C, Terraform</p>
                            
                            <p><em>Frameworks</em> .NET, React, Gatsby, Fastify, NodeJS, Django, Flask, Open Telemetry</p>
                        </div>
                        <div>                 
                            <p><em>Databases</em> SQL and NoSQL, mostly MongoDB and Oracle SQL</p>
                            <p><em>Tools</em> Jira, Git, LATEX, Microsoft Azure, GCP, Docker, Kubernetes (AKS), Feature Flags, TF Cloud, Honeycomb</p>
                        </div>
                    </div>                    
                </ul>
            </div>
        </div>
    );
}
