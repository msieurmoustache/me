import React from "react";

import Experience from '@components/CV/Experience';

export default function CV () {
    let jobs = [
        {
            timespan: "2021-2024",
            title: "Développeuse Staff, SRE",
            employer: "Workleap",
            description: "En tant que membre fondatrice de l'équipe SRE, je m'occupe principalement de faire en sorte que les SaaS de Workleap soient fiables, performants et sécuritaires en applicant les pratiques de la méthodologie SRE."
        },
        {
            timespan: "2018-2021",
            title: "Développeuse Full-Stack",
            employer: "Workleap (Officevibe)",
            description: "Membre de l'équipe Opérations, l'équipe était en charge du développement des outils opérationnels internes et d'accompagner les autres équipes de développement dans le déploiement de leurs fonctionnalités, ainsi que leurs investigations en production. "
        },
        {
            timespan: "2017-2018",
            title: "Analyste informatique",
            employer: "Institut National de Santé Publique du Québec",
            description: "Pilote opérationnel pour Système Généralisé d’Informations en Laboratoire (SGIL/LIMS). J'ai été en charge de l’assurance qualité, du support de niveau 3, des mises en production et de conseiller les pilotes d’orientation sur les développements futurs du logiciel."
        }
    ];

    let schooling = [
        {
            timespan: "2014-2017",
            name: "Baccalauréat en informatique et études des jeux vidéo",
            description: "Le contraste peut sembler majeur avec ma formation précédente, mais les concepts liés aux langues naturelles se transposent aisément aux langages de programmation.",
            school: "Université de Montréal"
        },{
            timespan: "2012-2014",
            name: "Baccalauréat en enseignement du français au secondaire",
            description: "Non complété. Les connaissances que j’y ai acquises m’ont quand même servie à la formation d’usagers et de bénévoles scouts.",
            school: "Université de Montréal"
        }
    ];

    let categoriesClasses = "text-xl font-bold bg-slate-200 dark:bg-slate-500 p-2 mt-5 mb-5";

    return (
        <div className="cv m-5" >
            <div className="w-9/12 m-auto">
                <header className="text-center">
                    <h1 className="text-2xl font-bold">Félixe Bélanger-Robillard</h1>
                    <p>(514) 431-9958 – f.brobillard@gmail.com</p>
                    <p>
                        <a className="text-blue-600" href="https://moussetache.ca/" target="_blank" rel="noreferrer noopener">moussetache.ca</a>
                    </p>
                </header>
    
                <h2 className={categoriesClasses}>Expérience professionnelle</h2>               
    
                <ul>
                    {jobs.map(x => <Experience timespan={x.timespan} name={x.title} place={x.employer} description={x.description} />)}
                </ul>
                
                <h2 className={categoriesClasses}>Formation</h2>
    
                <ul>
                    {schooling.map(x => <Experience timespan={x.timespan} name={x.name} place={x.school} description={x.description} />)}
                </ul>
    
                <h2 className={categoriesClasses}>Bénévolat</h2>
    
                <ul>
                    <Experience 
                    timespan={"Depuis 2010"} 
                    name={"Scout"} 
                    place={"Association des aventuriers de Baden-Powell"} 
                    description={<span>
                            Présentement membre du conseil de gestion, je suis en charge du développement du <a href="https://github.com/msieurmoustache/gaabp" target="_blank" rel="noopener noreferrer">site web</a>. Par le passé, j'ai été responsable de la formation de l'ensemble des bénévoles de l'Association et ai déjà siégé au conseil d'administration. J'ai également été responsable de l'ensemble des unités d'âge <em>castor</em>, en plus de m'impliquer localement à l'Île-Bizard, dans St-Michel et dans Outremont.
                        </span>} />
                </ul>
            </div>
    
            <div className="w-9/12 m-auto">
                <h2 className={categoriesClasses}>Abilités</h2>    
                <ul className="mt-0">
                    <div className="flex">
                        <div className="w-7/12 pr-1">
                            <p><em>Langues</em> Français, Anglais</p>
    
                            <p><em>Langages</em> JavaScript, C#, Java, Python, MUMPS, C</p>
                            
                            <p><em>Frameworks</em> .NET, React, Gatsby, Fastify, NodeJS, Django</p>
                        </div>
                        <div>                 
                            <p><em>Bases de données</em> SQL et NoSQL, principalement MongoDB et Oracle SQL</p>
                            <p><em>Outils</em> Jira, Git, LATEX, Microsoft Azure</p>
                        </div>
                    </div>                    
                </ul>
            </div>
        </div>
    );
}
