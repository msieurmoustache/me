import React, { useEffect, useState } from "react";

import Layout from "@components/Layout/Layout";
import ShowcaseCard from "@me/components/Showcase/ShowcaseCard";
import Title from "@me/components/Layout/Title";
import Checkbox from "@me/components/Checkbox";

export default function IndexPage () {
    let nightStarters = ["bouldering", "programming", "try outfits"];

    let foods = ["sushi", "[replace] bowls", "chicken bbq", "waffles", "bagel"];

    let hotBeverages = ["coffee", "green tea", "chai latte", "black tea", "sparkling water"];

    let stims = ["cube", "infinicube", "water bowl", "ring"];

    let selfCare = ["nails", "hair mask", "face mask", "practice makeup"];

    let nightEnders = ["minecraft", "sims 4", "baldur's gate 3", "read sci-fi", "read horror"];

    let activityTypes = {"Night Starters": nightStarters, "Meals": foods, "Beverages": hotBeverages, "Stims": stims, "Self-Care": selfCare, "Night Enders": nightEnders};

    let activityKeys = Object.keys(activityTypes);

    const [activitiesToBeRandomized, setActivities] = useState(activityKeys);

    function getRandom(array: string[]) {
        if(array) {
            let random = Math.floor(Math.random() * array.length);

            return array[random];
        }
    }

    const generateResults = () => {
      return activitiesToBeRandomized.map((activity) => {
        return getRandom(activityTypes[activity])
      })
    }

    const [results, setResults] = useState(generateResults());

    const handleCheckbox = (activity:string) => {
      console.log(activity)
      activitiesToBeRandomized.includes(activity) ? setActivities(activitiesToBeRandomized.filter(x => x !== activity)) : setActivities([...activitiesToBeRandomized, activity])
    };

    useEffect(() => {
      setResults(generateResults())
    }, [activitiesToBeRandomized])

  return (
    <Layout>
      <div className="p-3 m-auto w-11/12 flex flex-col items-center gap-5 mb-20">
          <div>
            As the need to self-regulate makes itself felt, I designed that small randomizer to taylor nights where I can take care of myself by focusing on my interests. 
          </div>        
          <div>
            I have problems regulating my nervous system, having dedicated alone time where I'm not concerned with being productive or masking helps with my self-regulation and also helps prevent autistic burnout.
          </div>
          <Title>Let's play what should I do tonight</Title>
          <ShowcaseCard>
            <Title>Select categories that need to be randomized</Title>
            {activityKeys.map(x => {
              return <Checkbox text={x} value={activitiesToBeRandomized.includes(x)} onClick={() => handleCheckbox(x)} />
            })}
          </ShowcaseCard>
          <Title>Results</Title>
          <ShowcaseCard>
            <div className="grid grid-cols-4 gap-3">
                {results.map((x, i) => {
                    return <ShowcaseCard key={i}>{x}</ShowcaseCard>;
                })}
            </div>
          </ShowcaseCard>
          <Title>Full list of possibillities</Title>
          {activityKeys.map((activity) => {
            return (
            <ShowcaseCard>
              <Title>{activity}</Title>
              <div className="grid grid-cols-4 gap-3">
                {activityTypes[activity].map((x, i) => {
                    return <ShowcaseCard key={i}>{x}</ShowcaseCard>;
                })}
              </div>
            </ShowcaseCard>
          )
          })}
        </div>
    </Layout>
  );
} 