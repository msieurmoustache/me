import React from "react";
import { Link } from "gatsby";
import { IoLogoGitlab } from "@react-icons/all-files/io5/IoLogoGitlab";
import { IoLogoGithub } from "@react-icons/all-files/io5/IoLogoGithub";
import { IoLogoLinkedin } from "@react-icons/all-files/io5/IoLogoLinkedin";
import { IoLogoInstagram } from "@react-icons/all-files/io5/IoLogoInstagram";

import SocialMediaButton from "@components/social-media/SocialMediaButton";

type props = {
  siteTitle : String
};

function Header ({ siteTitle } : props ) {
  let socialClassNames='fill-pink-600 scale-150';

  return (
    <header className="flex flex-col items-center gap-5 dark:text-white dark:bg-slate-800 mb-5">      
      <div className="flex gap-10 m-5">
        <SocialMediaButton href="https://github.com/mousse-tache">
          <IoLogoGithub className={socialClassNames} />
        </SocialMediaButton>
        <SocialMediaButton href="https://gitlab.com/moussetache">
          <IoLogoGitlab className={socialClassNames} />
        </SocialMediaButton> 
        <SocialMediaButton href="https://ca.linkedin.com/in/félixe-bélanger-robillard-7115a37b">
          <IoLogoLinkedin className={socialClassNames} />
        </SocialMediaButton> 
        <SocialMediaButton href="https://www.instagram.com/mousse.t.ache/">
          <IoLogoInstagram className={socialClassNames} />
        </SocialMediaButton> 
      </div>       
      <div>      
        <h2>
          <Link className="transition ease-in-out delay-150 text-3xl font-bold p-3 m-1 rounded hover:bg-gradient-to-b from-blue-400 via-pink-400 to-pink-white"  to="/">        
            {siteTitle}
          </Link>
        </h2>
      </div>
    </header>
  );
}

export default React.memo(Header);
